# Mandrill Hook #

This repo is a wrapper of mandrill-api for easiest usage.

## Usage: ##


## Send email using mandrill templates ##
```
#!javascript

var credentials = {
  'fromEmail': '<your email>',
  'fromName': '<your from name>',
  'apiKey': '<your mandrill api key>'
};

var templateEmail ={
    to: '<your recepient>',
    templateName: '<the name of your mandrill template>',
    templateContent: '<your email format template>',
    subject:'<subject email>',
    vars: [
      {'name': 'imgHeader', 'content': '<your header  image>'},
      {'name': 'headerHeight', 'content': '<your header height>'}, 
      {'name': 'footer', 'content': '<Your footer copy>'}
    ],
    templateVars:'<vars implentates in your templateContent>'>
};
```
The location of the file must be in app/modules/mails and use .html file extension. 

### Implementation code: ###
```
#!javascript

var mandrill = require('cobuild-mandrill-hook')(credentials);
      
mandrill.send(templateEmail, function(err, result){
  console.log("Result should be success!:", result, 'and error should be null:',err);        

});
```

This hook has default templates generally used for sending email.

1) **message:** 

```
#!javascript

var templateEmail = {
    to: '<your recepient>',
    templateName: '<the name of your mandrill template>',
    templateContent: 'message',
    subject:'<subject email>',
    vars: [
      {'name': 'imgHeader', 'content': '<your header  image>'},
      {'name': 'headerHeight', 'content': '<your header height>'}, 
      {'name': 'footer', 'content': '<Your footer copy>'}
    ],
    templateVars: {
      greeting: <'greeting'>,
      user: <'user'>,
      message : <'your message content'>
    }
};
```

**Example:**

![Captura de pantalla de 2016-09-01 09-28-23.png](https://bitbucket.org/repo/pd957A/images/1805188022-Captura%20de%20pantalla%20de%202016-09-01%2009-28-23.png)

2) **messageLinkMessage:**

```
#!javascript
var templateEmail = {
    to: '<your recepient>',
    templateName: '<the name of your mandrill template>',
    templateContent: 'messageLinkMessage',
    subject:'<subject email>',
    vars: [
      {'name': 'imgHeader', 'content': '<your header  image>'},
      {'name': 'headerHeight', 'content': '<your header height>'}, 
      {'name': 'footer', 'content': '<Your footer copy>'}
    ],
    templateVars:{
      greeting: '<greeting>',
      user: '<user>',
      linkUrl: '<link>',
      linkName :  '<linkName>',
      preMessage: '<pre message>',
      posMessage: '<pos message>'
   }
};
```
**Example:** 
![Captura de pantalla de 2016-09-01 09-52-54.png](https://bitbucket.org/repo/pd957A/images/4163423542-Captura%20de%20pantalla%20de%202016-09-01%2009-52-54.png)


## Send email using plain text ##
```
#!javascript

var credentials = {
  'fromEmail': '<your email>',
  'fromName': '<your from name>',
  'apiKey': '<your mandrill api key>'
};

var plainText = {
  to: '<your recepient>',
  subject: '<your subject>',
  text: '<your content>'
};

var mandrill = require('cobuild-mandrill-hook')(credentials);
      
mandrill.send(plainText, function(err, result){
  console.log("Result should be success!:", result, 'and error should be null:',err);        

});

```

## Sending Attachments ##

To include attachments on your emails you have 2 options 

1. Indicate the local filePath of the file you want to send. This is suggested when the file is located in the same server where the mailer process runs

```

var templateEmail ={
    to: '<your recepient>',
    templateName: '<the name of your mandrill template>',
    templateContent: '<your email format template>',
    subject:'<subject email>',
    vars: [
      {'name': 'imgHeader', 'content': '<your header  image>'},
      {'name': 'headerHeight', 'content': '<your header height>'}, 
      {'name': 'footer', 'content': '<Your footer copy>'}
    ],
    templateVars:'<vars implentates in your templateContent>'>
    attachments: [
      {
        filePath: <local-file-path>`
      }
    ]
};

```

2. Include the content of the attachment as a base64 string, and the mime type of the attachment. This method is suggested if the file is located on a bucker or a different location, that the mailer process.

```

var templateEmail ={
    to: '<your recepient>',
    templateName: '<the name of your mandrill template>',
    templateContent: '<your email format template>',
    subject:'<subject email>',
    vars: [
      {'name': 'imgHeader', 'content': '<your header  image>'},
      {'name': 'headerHeight', 'content': '<your header height>'}, 
      {'name': 'footer', 'content': '<Your footer copy>'}
    ],
    templateVars:'<vars implentates in your templateContent>'>
    attachments: [
      {
        content: <base64-string-of-the-file>,
        type: '<mime-type-of-the-file>',
        name: '<name-of-file>'
      }
    ]
};
```


## Create a MailChimp list ##

```
#!javascript

var credentials = {
  'fromEmail': '<your email>',
  'fromName': '<your from name>',
  'apiKeyMC': '<your MailChimp api key>',

};

var createList = {
  'name': '<listname>',
  'contact': {
    'company': '<company name>',
    'address1': '<address1>',
    'address2': '<address2>',
    'city': '<city>',
    'state': '<state>',
    'zip': '<zip>',
    'country': '<cod-country>', //example: US, CO 
    'phone': '<phone>'
  },
  'permission_reminder': '<permission_reminder>',
  'use_archive_bar': true, //false
  'campaign_defaults': {
    'from_name': '<from_name>',
    'from_email': '<from_email>',
    'subject': '<subject>',
    'language': '<language>'
  },
  'notify_on_subscribe':'',
  'notify_on_unsubscribe':'',
  'visibility': 'pub', //prv
  'email_type_option': false //true

}

var mandrill = require('cobuild-mandrill-hook')(credentials);
      
mandrill.createList(createList, function(err, result){
  console.log("Result should be success!:", result, 'and error should be null:',err);        

});

```
## Add a member on MailChimp list ##


```
#!javascript

var credentials = {
  'fromEmail': '<your email>',
  'fromName': '<your from name>',
  'apiKeyMC': '<your MailChimp api key>',

};

var createContact = {
  'name': '<listname>',
  'status':'<status>', //subscribed, unsubscribed, cleaned, pending
  'address-email': '<address-email>'
}

var mandrill = require('cobuild-mandrill-hook')(credentials);
      
mandrill.createContact(createContact, function(err, result){
  console.log("Result should be success!:", result, 'and error should be null:',err);        

});

```