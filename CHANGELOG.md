# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## [2.1.1] - 2021-03-26
### Bug Fix
Fixed bug when empty attachments

## [2.1.0] - 2021-03-20
### Added
Attachment support, local file path or inline base64 string

## [2.0.0] - 2016-09-22
### Added
Cobuild.json to support CLI operations

## [1.0.0] - 2016-03-10

First Version