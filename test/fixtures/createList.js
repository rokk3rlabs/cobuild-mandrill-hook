module.exports = {
  "name": "contact list test",
  "contact": {
    "company": "Rokk3r Labs",
    "address1": "1680 Michigan Avenue",
    "address2": "Unit 815",
    "city": "Miami",
    "state": "FL",
    "zip": "33139",
    "country": "US",
    "phone": ""
  },
  "permission_reminder": "List",
  "use_archive_bar": true,
  "campaign_defaults": {
    "from_name": "Rokk3r",
    "from_email": "doe@rokk3rlabs.com",
    "subject": "",
    "language": "en"
  },
  "notify_on_subscribe":"",
  "notify_on_unsubscribe":"",
  "visibility": "pub",
  "email_type_option": false

}