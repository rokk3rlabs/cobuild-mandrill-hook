module.exports = {
  to: 'natalia.castellanos@rokk3rlabs.com',
  subject: 'Mandrill Hook HTML Test',
  templateName: 'email-templete',
  templateContent: 'messageLinkMessage',
  templateVars:{
  	title : "Hello Word",
  	message : "This is a message"
  },
  vars:[
        {
            "name": "sample",
            "content": "sample content"
        }
      ]
};