var debug   = require('debug')('cobuild-aws-hook');
var should = require('should');

describe('Mandrill Hook', function(){
  
 
  var fixtures = {};

  before(function (done) {

      loadFixtures();
      done();
  });

 it('Should create an instance ', function (done) {
      var mandrill = require(process.cwd()+'/index')(fixtures.credentials);
      debug(mandrill);
      mandrill.should.ok()
      done();
  });

   it('Should send a plain text email ', function (done) {
      var mandrill = require(process.cwd()+'/index')(fixtures.credentials);
      debug(mandrill);
      mandrill.send(fixtures.plainText, function(err, result){
        should.not.exist(err);
        result.should.ok()
        done();
      });
      
  });

  it('Should send a email template', function (done) {
      var mandrill = require(process.cwd()+'/index')(fixtures.credentials);

      debug(mandrill);
      mandrill.send(fixtures.emailTemplete, function(err, result){
        should.not.exist(err);
        result.should.ok()
        done();
      });
      
  });
  it('Should create a list in mailchimp', function (done) {
      var mandrill = require(process.cwd()+'/index')(fixtures.credentials);

      debug(mandrill);
      mandrill.createList(fixtures.createList, function(err, result){
        should.not.exist(err);
        result.should.ok()
        done();
      });
  });

  it('Should create a contact in list on mailchimp', function (done) {
      var mandrill = require(process.cwd()+'/index')(fixtures.credentials);

      debug(mandrill);
      mandrill.createContact(fixtures.createContact, function(err, result){
        should.not.exist(err);
        result.should.ok()
        done();
      });
  });

  function loadFixtures(argument) {

    var Cobuild = require('cobuild2');
    var path = require('path');

    Cobuild.Utils.Files.listFilesInFolder(__dirname + '/fixtures')
    .forEach(function (file) {
        var completePath =  path.join(__dirname , 'fixtures',  file)
        fixtures[file.slice(0, -3)] = require(completePath);
        
    });
  }


});


